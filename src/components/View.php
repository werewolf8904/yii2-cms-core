<?php

namespace werewolf8904\cmscore\components;

use yii\base\ViewEvent;
/**
 * 
 */
class View extends \yii\web\View
{
    /**
     * @event Event an event that is triggered by [[beforePageRender()]].
     */
    public const EVENT_BEFORE_PAGE_RENDER= 'beforePageRender';

    /**
     * Called by FrontendController before render is calling.
     *
     * @param $viewFile
     * @param $params
     */
    public function beginPageRender($viewFile, $params)
    {
        $event = new ViewEvent([
            'viewFile' => $viewFile,
            'params' => $params,
        ]);
        $this->trigger(self::EVENT_BEFORE_PAGE_RENDER,$event);
    }
}
