<?php

namespace werewolf8904\cmscore\migrations;

use werewolf8904\cmscore\db\Migration;

/**
 * Class M180808073754Languagege
 */
class M180808073754Language extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = $this->tableOptions;
        $this->createTable('{{%language}}', [
            'code' => $this->string()->notNull(),
            'name' => $this->string()->notNull(),
            'status' => $this->smallInteger()->notNull()->defaultValue(1),
            'sort' => $this->smallInteger()->notNull()->defaultValue(1),
        ], $tableOptions);
        $this->addPrimaryKey('language-code', '{{%language}}', 'code');

        $this->batchInsert(
            '{{%language}}',
            ['name', 'code', 'status', 'sort',],
            [
                ['Русский', 'ru', 1, 2,],
            ]
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%language}}');
    }
}
