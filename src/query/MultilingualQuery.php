<?php

namespace werewolf8904\cmscore\query;

use werewolf8904\cmscore\traits\MultilingualTrait;
use yii\db\ActiveQuery;

/**
 * Class MultilingualQuery
 * @package werewolf8904\cmscore\query
 */
class MultilingualQuery extends ActiveQuery
{
    use MultilingualTrait;
}
