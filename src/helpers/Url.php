<?php

namespace werewolf8904\cmscore\helpers;

use werewolf8904\urlfactory\IObjectUrlFactory;
use Yii;

/**
 * 
 */
class Url
{
    public static function getObjectUrl(object $model, $scheme = false)
    {
        return Yii::$container->get(IObjectUrlFactory::class)->getUrl($model, $scheme);
    }

    public static function getArrayUrl($key, $array, $scheme = false)
    {
        return Yii::$container->get(IObjectUrlFactory::class)->getUrlFromArray($key, $array, $scheme);
    }
}
