<?php

namespace werewolf8904\cmscore\helpers;

use code2magic\glide\components\IGlide;
use yii\di\Instance;

/**
 * 
 */
class Glide
{
    /**
     * @var IGlide
     */
    private static $_glide;

    /**
     * @return IGlide
     * @throws \yii\base\InvalidConfigException
     */
    public static function getGlide(): IGlide
    {
        if (self::$_glide === null) {
            self::$_glide = Instance::ensure(IGlide::class, IGlide::class);
        }
        return self::$_glide;
    }

    /**
     * @param array $params
     * @param bool  $scheme
     *
     * @return bool|string
     * @throws \yii\base\InvalidConfigException
     */
    public static function createSignedUrl(array $params, $scheme = false)
    {
        return self::getGlide()->createSignedUrl($params, $scheme);
    }

    /**
     * @param       $image
     * @param null  $preset
     * @param array $params
     *
     * @return string
     * @throws \yii\base\InvalidConfigException
     */
    public static function getGlideThumbnail($image, $preset = null, array $params = []): string
    {
        return self::getGlide()->getGlideThumbnail($image, $preset, $params);
    }
}
