<?php

namespace werewolf8904\cmscore\traits;

use Yii;
use yii\helpers\Inflector;

/**
 * Trait ModelLabelMultilingual
 * @package werewolf8904\cmscore\traits
 */
trait ModelLabelMultilingual
{
    public $message_category = 'model_labels';

    /**
     * Generates a user friendly attribute label based on the give attribute name.
     * This is done by replacing underscores, dashes and dots with blanks and
     * changing the first letter of each word to upper case.
     * For example, 'department_name' or 'DepartmentName' will generate 'Department Name'.
     *
     * @param string $name the column name
     *
     * @return string the attribute label
     */
    public function generateAttributeLabel($name)
    {
        return Yii::t($this->message_category, Inflector::camel2words($name, true));
    }
}
