<?php

namespace werewolf8904\cmscore\traits;

/**
 * Trait WithCurrentTranslation
 * @package werewolf8904\cmscore\traits
 */
trait WithCurrentTranslation
{


    /**
     * @return $this
     */
    public function joinWithCurrentTranslation()
    {

        /**
         * @var $model          \yii\db\ActiveRecord
         * @var $relation_model \yii\db\ActiveRecord
         */
        $model = new $this->modelClass;
        $relation = $model->getRelation($this->_relation);
        $relation_model = new $relation->modelClass();
        $attr = array_diff_key($relation_model->attributes, $model->attributes);
        $attr = array_keys($attr);
        $this->select($model::tableName() . '.*');
        $this->addSelect(
            $attr
        );
        return $this->joinWith($this->_relation,false);

    }
}
