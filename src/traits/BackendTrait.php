<?php

namespace werewolf8904\cmscore\traits;

use werewolf8904\cmscore\models\Language;
use werewolf8904\cmscore\query\MultilingualQuery;

/**
 * Trait MultilingualTrait
 *
 * @package backend\traits
 */
trait BackendTrait
{
    /**
     * @inheritDoc
     */
    public function transactions()
    {
        return ['default'=>static::OP_ALL,];
    }

    /**
     * @return MultilingualQuery
     */
    public static function find()
    {
        if (method_exists($query = parent::find(), 'multilingual')) {
            $query->languageField = 'language_code';
            return $query->multilingual(); //TODO: Fix this
        }
        return (new MultilingualQuery(static::class, ['languageField' => 'language_code',]))->multilingual();
    }

    /**
     * @inheritDoc
     */
    public function setAttributes($values, $safeOnly = true)
    {
        parent::setAttributes($values, $safeOnly);
        foreach ($values as $name => $value) {
            if ($this->hasMethod('hasLangAttribute') && $this->hasLangAttribute($name)) {
                $this->setLangAttribute($name, $value);
            }
        }
    }

    /**
     * @inheritDoc
     */
    public function getAttribute($value, $safeOnly = true)
    {
        if ($this->hasMethod('hasLangAttribute') && $this->hasLangAttribute($value)) {
            return $this->getLangAttribute($value);
        }
        return parent::getAttribute($value, $safeOnly);
    }
}
