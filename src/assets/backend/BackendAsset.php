<?php

namespace werewolf8904\cmscore\assets\backend;

use yii\web\AssetBundle;
use yii\web\JqueryAsset;

/**
 * 
 */
class BackendAsset extends AssetBundle
{
    /**
     * @inheritDoc
     */
    public $sourcePath = '@core/assets/backend';

    /**
     * @inheritDoc
     */
    public $js = [
        'js/core.js'
    ];

    /**
     * @inheritDoc
     */
    public $depends = [
        JqueryAsset::class
    ];
}