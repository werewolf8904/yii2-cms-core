"use strict";
(function ($) {
    var select2dependence = {
        selectmain: function (e) {
            var slave = $(this).parent().parent().parent().find('[data-slave]');
            slave.find('option').remove();
        }
    };
    var inputpreview = {
        global_images: [],
        input_preview_init: function (element) {
            element = typeof element != 'undefined' ? element : 'body';
            $(element).find('.input_preview').each(function (index) {
                if (typeof $(this).data('img') == 'undefined') {
                    var _this = $(this);
                    if (!_this.val()) {
                        var im = global_images.shift();
                        if (im) {
                            _this.val(im.replace(_this.data('storageSrc'), ''));
                        }

                    }
                    var img = $('<img/>', {
                        src: (_this.data('storageSrc') && _this.val().substr(0, 4) != 'http' ? _this.data('storageSrc') : '') + (_this.val() ? _this.val() : _this.data('defaultSrc')),
                        class: 'img-thumbnail',
                        width: 150
                    });

                    _this.data('img', img);
                    _this.parent().after(img);
                    _this.change(function () {
                        _this.val(_this.val().replace(_this.data('storageSrc'), ''));

                        img.attr('src', (_this.data('storageSrc') && _this.val().substr(0, 4) != 'http' ? _this.data('storageSrc') : '') + (_this.val() ? _this.val() : _this.data('defaultSrc')));
                        var val = _this.val();
                        if (val.indexOf(', ') !== -1) {

                            var images = val.split(', ');
                            _this.val(images.shift());

                            global_images = images;
                            var count = global_images.length;
                            for (var i = 0; i < count; i++) {
                                _this.parent().parent().parent().parent().parent().parent().find('.js-input-plus').trigger('click');
                            }
                            _this.trigger('change');


                        }

                    });
                }
            });
        }
    };
    $(function () {
        //init
        //tab open by anchor
        var url = document.location.toString();
        if (url.match('#')) {
            $('.nav-tabs a[href="#' + url.split('#')[1] + '"]').tab('show');
        }
        //Events binding

        //Select 2 dependency select
        $(document).on('select2:select', '[data-master]',
            select2dependence.selectmain);


        //Modal with ajax content
        $('.content').on('click', '.ajax', function (event) {
            event.preventDefault();
            var a = $(this);
            $('#color-dialog #ajax_content').load(a.attr('href'),
                function (response, status, xhr) {
                    if (status === 'success') {
                        $('#color-dialog').modal();
                    }
                });
        });
        $('.content').on('hide.bs.modal', '#color-dialog', function (event) {
            $(document).off('submit');
        });
        //Multiple input radio changed
        $('.multiple-input').on('click', '[data-is_main]',
            function (e) {
                var mul = $(e.delegateTarget);
                var radios = mul.find('[data-is_main]');
                $.each(radios, function (index, value) {
                    value.value = 0;
                    value.checked = false;
                });
                this.value = 1;
                this.checked = true;
                return true;
            });


        //Bulk button state changing
        $(document).on('change', '[type=checkbox]', function (e) {
            var ids = $('.grid-view').yiiGridView('getSelectedRows');

            if (ids.length > 0) {
                $('.bulk-button').attr('disabled', false);
            }
            else {
                $('.bulk-button').attr('disabled', true);
            }
        });
        //Bulk operation
        $(document).on('click', '.bulk-action a', function (e) {
            var ids = $('.grid-view').yiiGridView('getSelectedRows');
            var parent = $('.bulk-button');
            var _this = $(this).parent();
            var url = parent.data('url');
            var data = _this.data('data');
            var action = _this.data('action');
            var sender = function () {
                $.ajax({
                    type: 'POST',
                    url: url,
                    data: {"ids": ids, 'data': data, 'action': action},
                    proccessData: false,
                    success: function (data) {
                        if (data) {
                            var pjax_container = $('[data-pjax-container]');
                            var id = pjax_container.attr('id');
                            $.pjax.reload({container: '#' + id});
                        }
                    }

                });
            };
            yii.confirm(_this.data('message'), sender);
            return false;
        });


        //Stay on page
        $(document).on('click', 'button[name=stay]',
            function (e) {
                var hash = window.location.hash.substr(1);
                $(this).val(hash);
                return true;
            });


        inputpreview.input_preview_init();
        $(document).on('afterDeleteRow', '.multiple-input', function (event, row) {
            var mul = $(row);
            var radios = mul.find('[data-is_main]');
            if (radios.val() == 1) {
                var mi = $(event.currentTarget).find('[data-is_main]:first');
                if (mi.length > 0) {
                    mi[0].checked = true;
                    mi[0].value = 1;
                }
            }

            inputpreview.input_preview_init(event.target);
        });
        $(document).on('afterAddRow', '.multiple-input', function (event, row) {
            var mul = $(row);
            var radios = mul.find('[data-is_main]');
            var mi = $(event.currentTarget).find('[data-is_main]');

            if (mi.length > 1) {
                $.each(radios, function (index, value) {
                    value.value = 0;
                    value.checked = false;

                });
            }
            else {
                $.each(radios, function (index, value) {
                    value.value = 1;
                    value.checked = true;


                });
            }
            inputpreview.input_preview_init(event.target);
        });
        $(document).on('shown.bs.modal', '.kv-editable-popover', function (event) {
            inputpreview.input_preview_init(event.target);
        });
    });
})(jQuery);
