<?php

namespace werewolf8904\cmscore\events;

use yii\base\Event;

class BackendControllerAjaxOutputEvent extends Event
{
    public $saved_model;
    public $output;
    public $key;
}
