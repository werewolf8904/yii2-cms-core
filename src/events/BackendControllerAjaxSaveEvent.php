<?php

namespace werewolf8904\cmscore\events;

use yii\base\Event;

/**
 * 
 */
class BackendControllerAjaxSaveEvent extends Event
{
    public $saved_model;
}
