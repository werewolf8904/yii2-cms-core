<?php

namespace werewolf8904\cmscore\bootstrap;

use yii\base\BootstrapInterface;
use Yii;

/**
 * 
 */
class Bootstrap  implements BootstrapInterface
{
    public function bootstrap($app)
    {
        if (!Yii::$container->hasSingleton(\werewolf8904\composite\IServiceFactory::class)) {
            Yii::$container->setSingleton(\werewolf8904\composite\IServiceFactory::class, function () {
                return new \werewolf8904\composite\ServiceFactory([]);
            });
        }
        if (!Yii::$container->hasSingleton(\werewolf8904\urlfactory\IObjectUrlFactory::class)) {
            Yii::$container->setSingleton( \werewolf8904\urlfactory\IObjectUrlFactory::class, function () {
                return new \werewolf8904\urlfactory\ObjectUrlFactory([]);
            });
        }
    }
}