<?php

namespace werewolf8904\cmscore\controllers;

use werewolf8904\cmscore\behaviors\AjaxOutputImageBehavior;
use werewolf8904\cmscore\events\BackendControllerAjaxOutputEvent;
use werewolf8904\cmscore\events\BackendControllerAjaxSaveEvent;
use werewolf8904\cmscore\models\Language;
use werewolf8904\composite\IServiceFactory;
use werewolf8904\composite\service\BaseService;
use Yii;
use yii\db\ActiveRecord;
use yii\filters\VerbFilter;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\web\Controller as BaseController;
use yii\web\NotFoundHttpException;

/**
 * Class Controller
 *
 * @package modules/core
 */
abstract class BackendController extends BaseController
{
    public CONST AFTER_AJAX_SAVE = 'after_ajax_save';
    public CONST AJAX_OUT_FILTER = 'ajax_out_filter';

    /**
     * processing index controller view params before pass to render method
     *
     * @var \Closure
     *```php
     * function($params)
     *{
     *  return $params
     *}
     *```
     */
    public $index_view_param_filter;

    /**
     * processing create controller view params before pass to render method
     *
     * @var \Closure
     *```php
     * function($params)
     *{
     *  return $params
     *}
     *```
     */
    public $create_view_param_filter;

    /**
     * processing update controller view params before pass to render method
     *
     * @var \Closure
     *```php
     * function($params)
     *{
     *  return $params
     *}
     *```
     */
    public $update_view_param_filter;

    /**
     * if true after update or create  redirect to update action
     *
     * @var bool
     */
    public $stay_after_save = false;

    /**
     * name of post parameter that force $stay_after_save to true and contain anchor
     *
     * @var string
     */
    public $stay_post_parameter = 'stay';

    public $pk = 'id';
    /**
     * @var string
     */
    public $searchClass;

    /**
     * @var string
     */
    public $class;
    /**
     * Key for IServiceFactory
     *
     * @var BaseService
     */
    public $compositeService;
    /**
     * @var BaseService
     */
    protected $_compositeInstance;
    protected $_is_composite;

    public function init()
    {
        parent::init();
        $this->_is_composite = $this->compositeService ? true : false;
        if ($this->_is_composite) {
            $factory = Yii::$container->get(IServiceFactory::class);
            $this->_compositeInstance = $factory->getService($this->compositeService);
        }
    }

    /**
     * @param $action
     *
     * @return bool
     * @throws \yii\web\BadRequestHttpException
     */
    public function beforeAction($action)
    {
        if ($this->action->id === $this->defaultAction) {
            $this->remember();
        }
        $this->stay_after_save = array_key_exists($this->stay_post_parameter, Yii::$app->request->post()) ? true : $this->stay_after_save;
        return parent::beforeAction($action);
    }

    protected function remember()
    {
        Url::remember(Url::to(), static::class);
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['post',],
                ],
            ],
            'ajax_image' => [
                'class' => AjaxOutputImageBehavior::class,
            ],
        ];
    }

    /**
     * @return string
     * @throws \yii\base\InvalidArgumentException
     * @throws NotFoundHttpException
     * @throws \yii\base\InvalidConfigException
     */
    public function actionIndex()
    {
        $searchModel = Yii::createObject($this->searchClass);
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        // validate if there is a editable input saved via AJAX
        if (Yii::$app->request->post('hasEditable')) {
            // instantiate your book model for saving
            $id = Yii::$app->request->post('editableKey');
            $model = $this->findModel($id);
            $posted = current(Yii::$app->request->post($model->formName()));
            $post = [$model->formName() => $posted];
            $key = key($posted);
            if ($model->load($post) && $model->validate(array_keys($posted)) && $model->save(false)) {
                $this->afterAjaxSave($model);
                $output = $model->$key;
                $this->ajaxOutFilter($model, $output, $key);
                return Json::encode(['output' => $output, 'message' => '',]);
            }
            // return ajax json encoded response and exit
            return Json::encode(['output' => $model->oldAttributes[key($posted)], 'message' => $model->getErrors()[$key],]);
        }
        $params = compact('searchModel', 'dataProvider');
        $params = ($this->index_view_param_filter instanceof \Closure) ? \call_user_func($this->index_view_param_filter, $params) : $params;
        return $this->render('index', $params);
    }

    /**
     * Finds the model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     *
     * @param integer $id
     *
     * @return ActiveRecord the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        /**
         * @var $class ActiveRecord
         */
        $class = $this->class;
        $query = $class::find();
        if (($model = $query->andWhere([$this->pk => $id,])->one()) !== null) {
            return $model;
        }
        throw new NotFoundHttpException('The requested page does not exist.');
    }

    protected function afterAjaxSave($model)
    {
        $this->trigger($this::AFTER_AJAX_SAVE, new BackendControllerAjaxSaveEvent(['saved_model' => $model,]));
    }

    protected function ajaxOutFilter($model, &$output, $key)
    {

        $event = new BackendControllerAjaxOutputEvent(
            ['saved_model' => $model, 'output' => $output, 'key' => $key,]
        );
        $this->trigger($this::AJAX_OUT_FILTER, $event);
        $output = $event->output;
    }

    /**
     * @return string|\yii\web\Response
     * @throws \ReflectionException
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\db\Exception
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionCreate()
    {
        /**
         * @var $model ActiveRecord
         */
        $this->view->params['returnUrl'] = $this->getReturn();
        if (Yii::$app->request->isGet && $id = Yii::$app->request->get('id')) {
            if ($model = $this->_is_composite) {
                $model = $this->_compositeInstance->findToCopy($id);
                if (!$model) {
                    throw new NotFoundHttpException('The requested page does not exist.');
                }
            } else {
                $model = $this->findModel($id);
                $model->setIsNewRecord(true);
            }

        } else {
            $model = $this->_is_composite ? $this->_compositeInstance->create() : Yii::createObject($this->class);
        }
        if ($model->load(Yii::$app->request->post()) && ($this->_is_composite ? $this->_compositeInstance->save($model) : $model->save())) {
            if ($this->stay_after_save) {
                Yii::$app->session->setFlash('alert', [
                    'body' => Yii::t('backend', 'Successfully created'),
                    'options' => ['class' => 'alert alert-success',]
                ]);
                return $this->redirect([
                    'update',
                    'id' => $this->_is_composite ? $model->{$this->_compositeInstance->main_model}->id : $model->id,
                    '#' => Yii::$app->request->post($this->stay_post_parameter),
                ]);
            }

            return $this->goReturn();
        }
        $params = [
            'model' => $model,
            'languages' => Language::find()->andWhere(['status'=>1])->all(),
        ];
        $params = ($this->create_view_param_filter instanceof \Closure) ? \call_user_func($this->create_view_param_filter, $params) : $params;
        return $this->render('create', $params);
    }

    /**
     * @return array|null|string
     */
    protected function getReturn()
    {
        $previous = \yii\helpers\Url::previous(static::class);
        return ($previous !== null && $previous ? $previous : ['index',]);
    }

    /**
     * @return \yii\web\Response
     */
    protected function goReturn()
    {
        return $this->redirect($this->getReturn());
    }

    /**
     * @param $id
     *
     * @return string|\yii\web\Response
     * @throws \yii\db\Exception
     * @throws \yii\base\InvalidCallException
     * @throws \yii\base\InvalidArgumentException
     * @throws NotFoundHttpException
     */
    public function actionUpdate($id)
    {
        $this->view->params['returnUrl'] = $this->getReturn();
        $model = $this->_is_composite ? $this->_compositeInstance->find($id) : $this->findModel($id);
        if (!$model) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
        if ($model->load(Yii::$app->request->post()) && ($this->_is_composite ? $this->_compositeInstance->save($model) : $model->save())) {
            if ($this->stay_after_save) {
                Yii::$app->session->setFlash('alert', [
                    'body' => Yii::t('backend', 'Successfully saved'),
                    'options' => ['class' => 'alert alert-success',]
                ]);
                return $this->refresh('#' . Yii::$app->request->post($this->stay_post_parameter));
            }
            return $this->goReturn();
        }
        $params = [
            'model' => $model,
            'languages' => Language::find()->andWhere(['status'=>1])->all(),
        ];
        $params = ($this->update_view_param_filter instanceof \Closure) ? \call_user_func($this->update_view_param_filter, $params) : $params;
        return $this->render('update', $params);
    }

    /**
     * @param $id
     *
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDelete($id)
    {
        $this->_is_composite ? $this->_compositeInstance->delete($id) : $this->findModel($id)->delete();
        return $this->goReturn();
    }
}
