<?php

namespace werewolf8904\cmscore\controllers;

use werewolf8904\cmscore\components\View;
use Yii;
use yii\helpers\ArrayHelper;
use yii\web\Controller;

/**
 * Class FrontController
 *
 * @package frontend\components\controllers
 * @property View|\yii\web\View|\werewolf8904\cmscore\components\View $view The view object that can be used to render views or view files.
 */
class FrontendController extends Controller
{
    /**
     * @param $action
     *
     * @return bool
     * @throws \yii\web\BadRequestHttpException
     */
    public function beforeAction($action): bool
    {
        $parent_result = parent::beforeAction($action);

        if ($parent_result && Yii::$app->request->isGet && (!(Yii::$app->request->isAjax || Yii::$app->request->isPjax))) {

            if (Yii::$app->session->hasFlash('alert')) {
                $this->view->params['alert']['type'] = ArrayHelper::getValue(Yii::$app->session->getFlash('alert'), 'options');
                $this->view->params['alert']['body'] = ArrayHelper::getValue(Yii::$app->session->getFlash('alert'), 'body');
            }
        }

        return $parent_result;
    }

    /**
     * @param string $view
     * @param array  $params
     *
     * @return string
     */
    public function render($view, $params = [])
    {
        if ($this->view instanceof View) {
            $this->view->context = $this;
            $this->view->beginPageRender($view, $params);
        }
        return parent::render($view, $params);
    }
}
