<?php

namespace werewolf8904\cmscore\widgets;

use Yii;
use yii\base\Widget;
use yii\behaviors\CacheableWidgetBehavior;
use yii\caching\TagDependency;


/**
 * Class FormWidget
 *
 * @package frontend\widgets
 */
class FormWidget extends Widget
{

    public $view_item;

    public $form_class;

    public $scenario;

    public $tags = 'FormWidget';

    /**
     * @var array|string
     */
    public $assets = [];

    /**
     * @var array
     */
    public $form_default_values = [];

    public function init()
    {

        parent::init();
        if (\is_string($this->assets)) {
            $this->view->registerAssetBundle($this->assets);
        } elseif (\is_array($this->assets)) {
            foreach ($this->assets as $asset) {
                $this->view->registerAssetBundle($asset);
            }
        }


    }

    public function behaviors()
    {
        return [
            [
                'class' => CacheableWidgetBehavior::class,
                'cacheDuration' => 3600,
                'cacheKeyVariations' => [
                    Yii::$app->language,
                    $this->view_item,
                    $this->form_class,
                    $this->scenario,
                    $this->form_default_values
                ],
                'cacheDependency' => [
                    'class' => TagDependency::class,
                    'tags' => $this->tags
                ],
            ],
        ];
    }


    /**
     * @return string
     * @throws \yii\base\InvalidConfigException
     */
    public function run()
    {
        $model = Yii::createObject(array_merge([
                'class' => $this->form_class]
            , $this->form_default_values));
        $model->scenario = $this->scenario;
        $view = $this->view_item ?? $model->getView();
        return $this->render($view, ['model' => $model]);

    }
}
