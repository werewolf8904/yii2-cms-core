<?php

namespace werewolf8904\cmscore\behaviors;

use Closure;
use yii\base\Behavior;
use yii\db\ActiveRecord;

/**
 * 
 */
class AttributeWatchBehavior extends Behavior
{
    /**
     *
     * @var string
     */
    public $watch;

    /**
     * @var Closure
     */
    public $watch_condition;

    /**
     * 
     */
    public $target;

    /**
     * 
     */
    public $value;

    /**
     * @return array
     */
    public function events()
    {
        return [
            ActiveRecord::EVENT_BEFORE_INSERT => 'watching',
            ActiveRecord::EVENT_BEFORE_UPDATE => 'watching',
        ];
    }

    /**
     * @param $event \yii\base\ModelEvent
     *
     */
    public function watching()
    {
        /**
         * @var $model ActiveRecord
         */
        $model = $this->owner;
        if ($model->getDirtyAttributes([$this->watch]) && $this->checkWatch()) {
            $this->owner->{$this->target} = $this->getValue();
        }
    }

    protected function checkWatch()
    {
        if ($this->value instanceof Closure || (is_array($this->value) && is_callable($this->value))) {
            return call_user_func($this->value, $this->owner);
        }
        return $this->value;
    }

    protected function getValue()
    {
        if ($this->value instanceof Closure || (is_array($this->value) && is_callable($this->value))) {
            return call_user_func($this->value, $this->owner);
        }
        return $this->value;
    }
}
