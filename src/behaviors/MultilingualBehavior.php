<?php

namespace werewolf8904\cmscore\behaviors;

use Yii;
use yii\base\Behavior;
use yii\base\Exception;
use yii\base\InvalidConfigException;
use yii\base\ModelEvent;
use yii\base\UnknownPropertyException;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\helpers\Inflector;

/**
 * Class MultilingualBehavior
 *
 * @package werewolf8904\cmscore\behaviors
 * @property ActiveRecord $owner
 */
class MultilingualBehavior extends Behavior
{
    /**
     * Multilingual attributes
     *
     * @var array
     */
    public $attributes;

    /**
     * Available languages
     * It can be a simple array: array('fr', 'en') or an associative array: array('fr' => 'Fran�ais', 'en' => 'English')
     * For associative arrays, only the keys will be used.
     *
     * @var array
     */
    public $languages;

    /**
     * @var string the default language.
     * Example: 'en'.
     */
    public $defaultLanguage;

    /**
     * @var string the name of the translation table
     */
    public $tableName;

    /**
     * @var string the name of translation model class.
     */
    public $langClassName;

    /**
     * @var string if $langClassName is not set, it will be assumed that $langClassName is
     * get_class($this->owner) . $this->langClassSuffix
     */
    public $langClassSuffix = 'Lang';

    /**
     * @var string the name of the foreign key field of the translation table related to base model table.
     */
    public $langForeignKey;

    /**
     * @var string the prefix of the localized attributes in the lang table. Here to avoid collisions in queries.
     * In the translation table, the columns corresponding to the localized attributes have to be name like this:
     * 'l_[name of the attribute]' and the id column (primary key) like this : 'l_id' Default to ''.
     */
    public $localizedPrefix = '';

    /**
     * @var string the name of the lang field of the translation table. Default to 'language'.
     */
    public $languageField = 'language';

    /**
     * @var boolean if this property is set to true required validators will be applied to all translation models.
     * Default to false.
     */
    public $requireTranslations = false;

    /**
     * @var boolean whether to force deletion of the associated translations when a base model is deleted.
     * Not needed if using foreign key with 'on delete cascade'.
     * Default to true.
     */
    public $forceDelete = true;

    /**
     * @var boolean whether to dynamically create translation model class.
     * If true, the translation model class will be generated on runtime with the use of the eval() function so no
     * additional php file is needed. See {@link createLangClass()} Default to true.
     */
    public $dynamicLangClass = false;

    /**
     * @var boolean whether to abridge the language ID.
     * Default to true.
     */
    public $abridge = true;

    public $currentLanguage;

    private $ownerClassName;
    private $ownerPrimaryKey;
    private $langClassShortName;
    private $ownerClassShortName;
    private $langAttributes = [];

    /**
     * @inheritdoc
     */
    public function events()
    {
        return [
            ActiveRecord::EVENT_AFTER_FIND => 'afterFind',
            ActiveRecord::EVENT_AFTER_UPDATE => 'afterUpdate',
            ActiveRecord::EVENT_AFTER_INSERT => 'afterInsert',
            ActiveRecord::EVENT_AFTER_DELETE => 'afterDelete',
            ActiveRecord::EVENT_BEFORE_VALIDATE => 'beforeValidate',
        ];
    }

    /**
     * Relation to model translations
     *
     * @return ActiveQuery
     */
    public function getTranslations()
    {
        return $this->owner->hasMany($this->langClassName, [$this->langForeignKey => $this->ownerPrimaryKey,]);
    }

    /**
     * Relation to model translation
     *
     * @param $language
     *
     * @return ActiveQuery
     */
    public function getTranslation($language = null)
    {
        $language = $language ?: $this->getCurrentLanguage();
        return $this->owner->hasOne($this->langClassName, [$this->langForeignKey => $this->ownerPrimaryKey,])
            ->where([$this->languageField => $language,]);
    }

    /**
     * @return mixed|string
     */
    public function getCurrentLanguage()
    {
        return $this->currentLanguage;
    }

    /**
     * Handle 'beforeValidate' event of the owner.
     * @param ModelEvent $event
     */
    public function beforeValidate($event)
    {
        foreach ($this->attributes as $attribute) {
            $this->setLangAttribute($this->getAttributeName($attribute, $this->defaultLanguage), $this->getLangAttribute($attribute));
        }
        $errors = [];
        $translations = $this->indexByLanguage(ArrayHelper::getValue($this->owner->getRelatedRecords(), 'translations', []));
        foreach ($this->languages as $lang) {
            $defaultLanguage = $lang === $this->defaultLanguage;
            if (!isset($translations[$lang])) {
                /** @var \yii\db\ActiveRecord $translation */
                $translation = new $this->langClassName;
                $translation->{$this->languageField} = $lang;
                $translation->{$this->langForeignKey} = $this->owner->getPrimaryKey();
                $translations[$lang] = $translation;
            }
            foreach ($this->attributes as $attribute) {
                $attribute_name = $defaultLanguage ? $attribute : $this->getAttributeName($attribute, $lang);
                $translation_attribute_name = $this->localizedPrefix . $attribute;

                $translations[$lang]->{$translation_attribute_name} = $this->getLangAttribute($attribute_name);
                if (!$translations[$lang]->validate($translation_attribute_name)) {
                    $errors[$attribute_name] = $translations[$lang]->getErrors($translation_attribute_name);
                }
            }
        }
        if ($errors) {
            $event->isValid = false;
            $this->owner->addErrors($errors);
        }
        $this->owner->populateRelation('translations', $translations);
    }

    /**
     * @param string $name the name of the attribute
     * @param string $value the value of the attribute
     */
    public function setLangAttribute($name, $value)
    {
        $this->langAttributes[$name] = $value;
    }

    /**
     * @param $attribute
     * @param $language
     *
     * @return string
     */
    protected function getAttributeName($attribute, $language)
    {
        $language = $this->abridge ? $language : Inflector::camel2id(Inflector::id2camel($language), '_');
        return $attribute . '_' . $language;
    }

    /**
     * @param string $name the name of the attribute
     *
     * @return string the attribute value
     */
    public function getLangAttribute($name)
    {
        return $this->hasLangAttribute($name) ? $this->langAttributes[$name] : null;
    }

    /**
     * Whether an attribute exists
     *
     * @param string $name the name of the attribute
     *
     * @return boolean
     */
    public function hasLangAttribute($name)
    {
        return array_key_exists($name, $this->langAttributes);
    }

    /**
     * @param $records
     *
     * @return ActiveRecord[]
     */
    protected function indexByLanguage($records)
    {
        $sorted = [];
        foreach ($records as $record) {
            $sorted[$record->{$this->languageField}] = $record;
        }
        return $sorted;
    }

    /**
     * Handle 'afterFind' event of the owner.
     */
    public function afterFind()
    {
        /** @var ActiveRecord $owner */
        $owner = $this->owner;
        if ($owner->isRelationPopulated('translations') && $related = $owner->getRelatedRecords()['translations']) {
            $translations = $this->indexByLanguage($related);
            foreach ($this->languages as $lang) {
                foreach ($this->attributes as $attribute) {
                    foreach ($translations as $translation) {
                        if ($this->getLanguageBaseName($translation->{$this->languageField}) == $lang) {
                            $attributeName = $this->localizedPrefix . $attribute;
                            $this->setLangAttribute($this->getAttributeName($attribute, $lang), $translation->{$attributeName});

                            if ($lang == $this->defaultLanguage) {
                                $this->setLangAttribute($attribute, $translation->{$attributeName});
                            }
                        }
                    }
                }
            }
        } else {
            if (!$owner->isRelationPopulated('translation')) {
                $owner->translation;
            }

            $translation = $owner->getRelatedRecords()['translation'];
            if ($translation) {
                foreach ($this->attributes as $attribute) {
                    $attribute_name = $this->localizedPrefix . $attribute;
                    $owner->setLangAttribute($attribute, $translation->$attribute_name);
                }
            }
        }
        foreach ($this->attributes as $attribute) {
            if ($owner->hasAttribute($attribute) && $this->getLangAttribute($attribute)) {
                $owner->setAttribute($attribute, $this->getLangAttribute($attribute));
            }
        }
    }

    /**
     * @param $language
     *
     * @return string
     */
    protected function getLanguageBaseName($language)
    {
        return $this->abridge ? substr($language, 0, 2) : $language;
    }

    /**
     * Handle 'afterDelete' event of the owner.
     */
    public function afterDelete()
    {
        if ($this->forceDelete) {
            /** @var ActiveRecord $owner */
            $owner = $this->owner;
            $owner->unlinkAll('translations', true);
        }
    }

    /**
     * @inheritdoc
     */
    public function canGetProperty($name, $checkVars = true)
    {
        return method_exists($this, 'get' . $name) || ($checkVars && property_exists($this, $name))
            || $this->hasLangAttribute($name);
    }

    /**
     * @inheritdoc
     */
    public function canSetProperty($name, $checkVars = true)
    {
        return $this->hasLangAttribute($name);
    }

    /**
     * @inheritdoc
     */
    public function __get($name)
    {
        try {
            return parent::__get($name);
        } catch (UnknownPropertyException $e) {
            if ($this->hasLangAttribute($name)) return $this->getLangAttribute($name);
            // @codeCoverageIgnoreStart
            else throw $e;
            // @codeCoverageIgnoreEnd
        }
    }

    /**
     * @inheritdoc
     */
    public function __set($name, $value)
    {
        try {
            parent::__set($name, $value);
        } catch (UnknownPropertyException $e) {
            if ($this->hasLangAttribute($name)) $this->setLangAttribute($name, $value);
            // @codeCoverageIgnoreStart
            else throw $e;
            // @codeCoverageIgnoreEnd
        }
    }

    /**
     * @inheritdoc
     * @codeCoverageIgnore
     */
    public function __isset($name)
    {
        if (!parent::__isset($name)) {
            return $this->hasLangAttribute($name);
        }
        return true;
    }

    /**
     * Handle 'afterInsert' event of the owner.
     *
     * @throws \yii\base\Exception
     */
    public function afterInsert()
    {
        /** @var \yii\db\ActiveRecord $owner */
        $owner = $this->owner;
        if ($owner->isRelationPopulated('translations')) {
            $translations = $this->indexByLanguage($owner->getRelatedRecords()['translations']);
            $this->saveTranslations($translations);
        } elseif ($owner->isRelationPopulated('translation')) {
            $translations = $this->indexByLanguage([$owner->getRelatedRecords()['translation']]);
            $this->saveTranslations($translations);
        } else {
            $this->saveTranslations();
        }
    }

    /**
     * @param array $translations
     *
     * @throws \yii\base\Exception
     */
    private function saveTranslations($translations = [])
    {
        /** @var \yii\db\ActiveRecord $owner */
        $owner = $this->owner;
        $saved = true;
        $new_translations = [];
        foreach ($this->languages as $lang) {
            $defaultLanguage = $lang === $this->defaultLanguage;

            if (!isset($translations[$lang])) {
                /** @var \yii\db\ActiveRecord $translation */
                $translation = new $this->langClassName;
                $translation->{$this->languageField} = $lang;
            } else {
                $translation = $translations[$lang];
            }
            $translation->{$this->langForeignKey} = $owner->getPrimaryKey();
            $save = false;
            foreach ($this->attributes as $attribute) {
                $value = $defaultLanguage ? $owner->$attribute : $this->getLangAttribute($this->getAttributeName($attribute, $lang));

                if ($value !== null) {
                    $field = $this->localizedPrefix . $attribute;
                    $translation->$field = $value;
                    $save = true;
                }
            }
            if ($translation->isNewRecord && !$save){
                continue;
            }
            if (!$translation->save()) {
                foreach ($translation->getErrors() as $attr => $error) {
                    $attr = $defaultLanguage ? $attr : $this->getAttributeName($attr, $lang);
                    $owner->addError($attr, implode(';', $error));
                }
                $saved = false;
            }
            $new_translations[] = $translation;
        }

        if (!$saved) {
            throw new Exception('Multilingual not saved');
        }
        $this->owner->populateRelation('translations', $new_translations);
    }

    /**
     * @inheritdoc
     * @throws \yii\base\InvalidConfigException
     */
    public function attach($owner)
    {
        if (!($owner instanceof ActiveRecord)) {
            throw new InvalidConfigException('owner must be instance of ActiveRecord');
        }
        /** @var ActiveRecord $owner */
        parent::attach($owner);
        if (empty($this->languages) || !is_array($this->languages)) {
            throw new InvalidConfigException('Please specify array of available languages for the ' . get_class($this) . ' in the '
                . get_class($owner) . ' or in the application parameters', 101);
        }
        if (array_values($this->languages) !== $this->languages) { //associative array
            $this->languages = array_keys($this->languages);
        }
        $this->languages = array_unique(array_map(function ($language) {
            return $this->getLanguageBaseName($language);
        }, $this->languages));
        if (!$this->defaultLanguage) {
            $this->defaultLanguage = isset(Yii::$app->params['defaultLanguage']) && Yii::$app->params['defaultLanguage'] ?
                Yii::$app->params['defaultLanguage'] : Yii::$app->language;
        }
        $this->defaultLanguage = $this->getLanguageBaseName($this->defaultLanguage);
        if (!$this->currentLanguage) {
            $this->currentLanguage = $this->getLanguageBaseName(Yii::$app->language);
        }
        if (empty($this->attributes) || !is_array($this->attributes)) {
            throw new InvalidConfigException('Please specify multilingual attributes for the ' . get_class($this) . ' in the '
                . get_class($owner), 103);
        }
        if (!$this->langClassName) {
            $this->langClassName = get_class($owner) . $this->langClassSuffix;
        }
        $this->langClassShortName = $this->getShortClassName($this->langClassName);
        $this->ownerClassName = get_class($owner);
        $this->ownerClassShortName = $this->getShortClassName($this->ownerClassName);
        /** @var ActiveRecord $className */
        $className = $this->ownerClassName;
        $this->ownerPrimaryKey = $className::primaryKey()[0];
        if (!isset($this->langForeignKey)) {
            throw new InvalidConfigException('Please specify langForeignKey for the ' . get_class($this) . ' in the '
                . get_class($owner), 105);
        }
        if ($this->dynamicLangClass) {
            $this->createLangClass();
        }
        $translation = new $this->langClassName;
        foreach ($this->languages as $lang) {
            foreach ($this->attributes as $attribute) {
                $attributeName = $this->localizedPrefix . $attribute;
                $this->setLangAttribute($this->getAttributeName($attribute, $lang), $translation->{$attributeName});
                if ($lang == $this->defaultLanguage) {
                    $this->setLangAttribute($attribute, $translation->{$attributeName});
                }
            }
        }
    }

    /**
     * @param string $className
     *
     * @return string
     */
    private function getShortClassName($className)
    {
        return substr($className, strrpos($className, '\\') + 1);
    }

    /**
     * 
     */
    public function createLangClass()
    {
        if (!class_exists($this->langClassName, false)) {
            $namespace = substr($this->langClassName, 0, strrpos($this->langClassName, '\\'));
            eval('
            namespace ' . $namespace . ';
            use yii\db\ActiveRecord;
            class ' . $this->langClassShortName . ' extends ActiveRecord
            {
                public static function tableName()
                {
                    return \'' . $this->tableName . '\';
                }
            }');
        }
    }

    /**
     * Handle 'afterUpdate' event of the owner.
     *
     * @throws \yii\base\Exception
     */
    public function afterUpdate()
    {
        /** @var \yii\db\ActiveRecord $owner */
        $owner = $this->owner;
        if ($owner->isRelationPopulated('translations')) {
            $translations = $this->indexByLanguage($owner->getRelatedRecords()['translations']);
            $this->saveTranslations($translations);
        } elseif ($owner->isRelationPopulated('translation')) {
            $translations = $this->indexByLanguage([$owner->getRelatedRecords()['translation']]);
            $this->saveTranslations($translations);
        } else {
            $this->saveTranslations();
        }
    }

    /**
     * 
     */
    public function getMultilingualAttributes()
    {
        return $this->attributes;
    }
}