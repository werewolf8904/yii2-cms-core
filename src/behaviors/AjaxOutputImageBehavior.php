<?php

namespace werewolf8904\cmscore\behaviors;

use werewolf8904\cmscore\controllers\BackendController;
use werewolf8904\cmscore\events\BackendControllerAjaxOutputEvent;
use code2magic\glide\components\IGlide;
use Yii;
use yii\base\Behavior;
use yii\helpers\Html;

/**
 * 
 */
class AjaxOutputImageBehavior extends Behavior
{
    /**
     * 
     */
    public $name_filter = ['image', 'thumbnail',];

    /**
     * 
     */
    public function events()
    {
        return [
            BackendController::AJAX_OUT_FILTER => 'process',
        ];
    }

    /**
     * @param \werewolf8904\cmscore\events\BackendControllerAjaxOutputEvent $event
     *
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    public function process(BackendControllerAjaxOutputEvent $event)
    {
        if (\in_array($event->key, $this->name_filter, true)) {
            $url = Yii::$container->get(IGlide::class)->getImageSourceUrl($event->output);
            $event->output = Html::img($url, ['style' => 'width: 40%']);
        }
    }
}