<?php

namespace werewolf8904\cmscore\behaviors;

use code2magic\glide\components\IGlide;
use yii\base\Behavior;
use yii\helpers\ArrayHelper;

/**
 * Class GlideThumbnailBehavior
 *
 * @package common\behaviors
 */
class GlideThumbnailBehavior extends Behavior
{
    public $glide_route = 'glide/index';
    public $config = [];
    public $glide = IGlide::class;
    public $path = 'thumbnail';

    /**
     * @param array $config
     *
     * @return mixed
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    public function getGlideThumbnail(array $config = [])
    {
        $glide = [
            $this->glide_route,
            'path' => $this->owner->{$this->path}
        ];
        $glide = ArrayHelper::merge($glide, $this->config);
        $glide = ArrayHelper::merge($glide, $config);
        return \Yii::$container->get($this->glide)->createSignedUrl($glide, true);
    }
}
