<?php

namespace werewolf8904\cmscore\behaviors;

use yii\base\Behavior;
use yii\db\ActiveRecord;

/**
 * Class JsonBehavior.
 * Behavior for encoding and decoding model fields as JSON.
 *
 * ```php
 *
 * public function behaviors()
 * {
 *     return [
 *          [
 *               'class' => JsonBehavior::class,
 *               'fields' => ['name_of_field#1', 'name_of_field#2',],
 *          ],
 *     ];
 * }
 */
class TimeBehavior extends Behavior
{
    public $fields = [];
    public $default = [];

    /**
     * @return array
     */
    public function events()
    {
        return [
            ActiveRecord::EVENT_BEFORE_INSERT => 'encode',
            ActiveRecord::EVENT_BEFORE_UPDATE => 'encode',
        ];
    }

    /**
     * 
     */
    public function encode()
    {
        $model = $this->owner;
        foreach ($this->fields as $field) {
            if (isset($model->$field)) {
                if (!\is_int($model->$field)) {
                    $model->$field = strtotime($model->$field);
                }
            }
        }
    }
}
