<?php

namespace werewolf8904\cmscore\models;

use werewolf8904\cmscore\behaviors\CacheInvalidateBehavior;
use Yii;
use yii\caching\TagDependency;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%language}}".
 *
 * @property integer $id
 * @property string  $name
 * @property string  $code
 * @property integer $status
 * @property integer $sort
 *
 */
class Language extends ActiveRecord
{
    protected static $_languages;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%language}}';
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'cacheInvalidate' => [
                'class' => CacheInvalidateBehavior::class,
                'tags' => [self::class,],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'code',], 'required',],
            [['status', 'sort',], 'integer',],
            [['name', 'code',], 'string', 'max' => 255,],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('model_labels', 'ID'),
            'name' => Yii::t('model_labels', 'Name'),
            'code' => Yii::t('model_labels', 'Code'),
            'status' => Yii::t('model_labels', 'Status'),
            'sort' => Yii::t('model_labels', 'Sort'),
        ];
    }

    /**
     * @return mixed
     */
    public static function getLanguages()
    {
        if (empty(static::$_languages)) {
            static::$_languages = Yii::$app->cache->getOrSet('languages', function () {
                return Language::find()->select('name')->andWhere([self::tableName().'.status'=>1])->indexBy('code')->asArray()->column();
            }, 0, new TagDependency(['tags' => [self::class,],]));
        }
        return static::$_languages;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public static function find()
    {
        return parent::find()->orderBy(['sort' => SORT_DESC]);
    }
}
