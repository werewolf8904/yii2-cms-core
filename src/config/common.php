<?php
return [
    'container' => [
        'singletons' => [
            \werewolf8904\composite\IServiceFactory::class=> function () {
                return new \werewolf8904\composite\ServiceFactory([]);
            },
            \werewolf8904\urlfactory\IObjectUrlFactory::class => [
                'class' => \werewolf8904\urlfactory\ObjectUrlFactory::class,
                'map' => [
                ],
            ],
            \werewolf8904\cmscore\language\ILanguage::class=>werewolf8904\cmscore\language\Language::class
        ]
    ],
];
