<?php

namespace werewolf8904\cmscore\form;

use Yii;
use yii\base\Model;
/**
 * 
 */
abstract class BaseForm extends Model implements \JsonSerializable
{
    /**
     * @var string
     */
    public $view_file;

    /**
     * @var string
     */
    protected $success_message;

    /**
     * @return array
     */
    public function jsonSerialize(): array
    {
        return [
            'success' => $this->hasErrors(),
            'reload' => 0,
            'errors' => $this->getErrors(),
            'attributes' => $this->attributes
        ];
    }

    /**
     * @return string
     */
    public function __toString()
    {
        try {
            return Yii::$app->view->renderAjax($this->getView(), ['model' => $this]);
        } catch (\Exception $e) {
            return '';
        }

    }

    public function getView(): string
    {
        return $this->view_file;
    }
}