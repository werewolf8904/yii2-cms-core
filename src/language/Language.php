<?php

namespace werewolf8904\cmscore\language;

use werewolf8904\cmscore\models\Language as L;
use Yii;

/**
 * 
 */
class Language implements ILanguage
{
    public function getLanguages()
    {
        return L::getLanguages();
    }

    public function getCurrentLanguageCode()
    {
        return Yii::$app->language;
    }
}
