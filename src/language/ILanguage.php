<?php

namespace werewolf8904\cmscore\language;

/**
 *
 */
interface ILanguage
{
    /**
     * Key is language key
     * Value language fullname
     *
     * @return array
     */
    public function getLanguages();

    public function getCurrentLanguageCode();
}