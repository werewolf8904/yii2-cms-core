<?php

use kartik\dynagrid\DynaGrid;
use yii\helpers\Html;

/**
 * @var $this yii\web\View
 * @var $searchModel \yii\db\ActiveRecord
 * @var $columns array
 * @var $action_column array
 * @var $title string
 * @var $title_create string
 * @var $dataProvider yii\data\ActiveDataProvider
 */

$this->title = $title;
$this->params['breadcrumbs'][] = $this->title;

?>
<div id="color-dialog" role="dialog" class="modal my-modal fade popup">
    <div class="modal-dialog modal-lg">
        <div class="modal-content" style="padding: 15px;">
            <div data-dismiss="modal" class="remove"></div>
            <div id="ajax_content">

            </div>
        </div>
    </div>
</div>
<div class="catalog-product-index">
    <?php
    $columns[] = \yii\helpers\ArrayHelper::merge(
        [
            'class' => \yii\grid\ActionColumn::class,
            'template' => '{update} {copy} {delete}',
            'buttons' => [
                'copy' => function ($url, $model, $key) {
                    return Html::a('<span class="glyphicon glyphicon-copy"></span>', ['create', 'id' => $model->id], ['title' => Yii::t('backend', 'Copy'), 'data-pjax' => 0]);
                }
            ]
        ],
        $action_column ?? []
    );
    $action_number = count($columns) - 1;
    $export = \kartik\export\ExportMenu::widget([
        'dataProvider' => $dataProvider,
        'columns' => $columns,
        'hiddenColumns' => [0, $action_number],
        'noExportColumns' => [0, $action_number],
        'timeout' => 180,
        'target' => \kartik\export\ExportMenu::TARGET_BLANK,
        'filename' => 'products',
        'batchSize' => 10
    ]); ?>
    <?= DynaGrid::widget([
        'moduleId' => 'dynagrid',
        'storage' => DynaGrid::TYPE_COOKIE,
        'theme' => 'panel-info',
        'showPersonalize' => true,
        'columns' => $columns,
        'options' => [
            'id' => !empty($id) ? $id : ('dg-' . \yii\helpers\Inflector::slug($title)),
        ],
        'gridOptions' => [
            'responsiveWrap' => false,
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'persistResize' => true,
            'pjax' => true,
            'resizableColumns' => true,
            'resizableColumnsOptions' => ['resizeFromBody' => true],
            //'floatHeader' => true,
            'floatHeaderOptions' => ['scrollingTop' => '50'],
            'panel' => [
                'after' => false
            ],
            'toolbar' => [
                '{export}',
                $export,
                ['content' =>
                    Html::a(Yii::t('backend', 'Create {modelClass}', [
                        'modelClass' => $title_create
                    ]), ['create'], ['class' => 'btn btn-success', 'data-pjax' => 0]) .
                    Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['index'], ['data-pjax' => 0, 'class' => 'btn btn-default', 'title' => Yii::t('backend', 'Reset Grid')])
                ],
                ['content' => '{dynagridFilter}{dynagridSort}{dynagrid}'],
            ],
        ],
    ]); ?>
</div>
